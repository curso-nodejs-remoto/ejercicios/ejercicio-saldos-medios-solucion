require("dotenv").config();
const clientes = require("./clientes.json").clientes;

const minimoSaldoMedio = 2000;
const moneda = process.env.NODE_PAIS === "US" ? "$" : "€";

for (const cliente of clientes.filter(cliente => cliente.telefonos.fijo !== "")) {
  console.log(cliente.dni);
}

function getPobres(clientes) {
  return clientes
    .filter(cliente => cliente.nomina && cliente.saldoMedio < minimoSaldoMedio)
    .map(cliente => ({
      dni: cliente.dni,
      nombreApellidos: `${cliente.nombre} ${cliente.apellidos}`,
      saldoMedio: cliente.saldoMedio
    }));
}

function getMediaSaldosMedio(clientes = []) {
  return clientes.reduce((acc, cliente) => acc + cliente.saldoMedio / clientes.length, 0);
}

const mediaSaldosMedios = getMediaSaldosMedio(getPobres(clientes));

console.log(`El saldo medio promedio de los clientes con nómina y con saldo medio menor a ${minimoSaldoMedio + moneda} es: ${mediaSaldosMedios + moneda}`);
